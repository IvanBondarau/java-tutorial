## Introduction to Java
This repository contains solutions to the problems in the "Introduction to Java" course. The course was conducted by the EPAM SYSTEMS laboratory and was completed in the summer of 2019.

Course assignment conditions can be found at: https://github.com/Java0Tutor